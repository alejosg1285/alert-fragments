package com.alejosaag.tallerbarfragment;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.alejosaag.tallerbarfragment.Fragments.ImparFragment;
import com.alejosaag.tallerbarfragment.Fragments.MathsFragment;
import com.alejosaag.tallerbarfragment.Fragments.NaturalFragment;
import com.alejosaag.tallerbarfragment.Fragments.StudentsFragment;
import com.alejosaag.tallerbarfragment.Fragments.TableFragment;

public class MainActivity extends AppCompatActivity implements MathsFragment.OnFragmentInteractionListener, StudentsFragment.OnFragmentInteractionListener, TableFragment.OnFragmentInteractionListener, ImparFragment.OnFragmentInteractionListener, NaturalFragment.OnFragmentInteractionListener {
    private MathsFragment mathsFragment;
    private StudentsFragment studentsFragment;
    private TableFragment tableFragment;
    private NaturalFragment naturalFragment;
    private ImparFragment imparFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mathsFragment = new MathsFragment();
        studentsFragment = new StudentsFragment();
        tableFragment = new TableFragment();
        naturalFragment = new NaturalFragment();
        imparFragment = new ImparFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.fragmentLayout, mathsFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        switch (item.getItemId()) {
            case R.id.operac:
                ft.replace(R.id.fragmentLayout, mathsFragment);
                break;
            case R.id.students:
                ft.replace(R.id.fragmentLayout, studentsFragment);
                break;
            case R.id.tablet:
                ft.replace(R.id.fragmentLayout, tableFragment);
                break;
            case R.id.natural:
                ft.replace(R.id.fragmentLayout, naturalFragment);
                break;
            case R.id.parimpar:
                ft.replace(R.id.fragmentLayout, imparFragment);
                break;
            default:
                break;
        }

        ft.commit();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
