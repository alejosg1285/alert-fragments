package com.alejosaag.tallerbarfragment.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.alejosaag.tallerbarfragment.R;

import java.text.DecimalFormat;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StudentsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StudentsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StudentsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private EditText txtStudent1a;
    private EditText txtSignature1;
    private EditText txtScore1a;
    private EditText txtScore2a;
    private EditText txtScore3a;
    private EditText txtStudent2a;
    private EditText txtSignature2;
    private EditText txtScore1b;
    private EditText txtScore2b;
    private EditText txtScore3b;
    private Button btnScores;

    public StudentsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StudentsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StudentsFragment newInstance(String param1, String param2) {
        StudentsFragment fragment = new StudentsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_students, container, false);

        txtStudent1a = view.findViewById(R.id.txtStudent1);
        txtSignature1 = view.findViewById(R.id.txtSignature1);
        txtScore1a = view.findViewById(R.id.score1a);
        txtScore2a = view.findViewById(R.id.score1a);
        txtScore3a = view.findViewById(R.id.score3a);
        txtStudent2a = view.findViewById(R.id.txtSignature2);
        txtSignature2 = view.findViewById(R.id.txtSignature2);
        txtScore1b = view.findViewById(R.id.score1b);
        txtScore2b = view.findViewById(R.id.score2b);
        txtScore3b = view.findViewById(R.id.score3b);
        btnScores = view.findViewById(R.id.btnCalcScores);
        btnScores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String studentName1 = txtStudent1a.getText().toString();
                String studentSignature1 = txtSignature1.getText().toString();
                float studenScore1a = Float.parseFloat(txtScore1a.getText().toString());
                float studenScore2a = Float.parseFloat(txtScore2a.getText().toString());
                float studenScore3a = Float.parseFloat(txtScore3a.getText().toString());
                String studentName2 = txtStudent2a.getText().toString();
                String studentSignature2 = txtSignature2.getText().toString();
                float studenScore1b = Float.parseFloat(txtScore1b.getText().toString());
                float studenScore2b = Float.parseFloat(txtScore2b.getText().toString());
                float studenScore3b = Float.parseFloat(txtScore3b.getText().toString());
                float finalScore1 = (studenScore1a + studenScore2a + studenScore3a) / 3;
                float finalScore2 = (studenScore1b + studenScore2b + studenScore3b) / 3;

                DecimalFormat f = new DecimalFormat("##.0");

                String meg = String.format("Aprendiz: %s \n Materia: %s \n nota 1: %s \n nota 2: %s \n nota 3: %s \n final: %s \n\n", studentName1, studentSignature1, f.format(studenScore1a), f.format(studenScore2a), f.format(studenScore3a), f.format(finalScore1));
                meg += String.format("Aprendiz: %s \n Materia: %s \n nota 1: %s \n nota 2: %s \n nota 3: %s \n final: %s", studentName2, studentSignature2, f.format(studenScore1b), f.format(studenScore2b), f.format(studenScore3b), f.format(finalScore2));

                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Estudiantes y notas");
                alert.setMessage(meg);
                alert.setCancelable(false);
                alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.show();
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
