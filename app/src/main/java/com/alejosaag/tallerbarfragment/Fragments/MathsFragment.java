package com.alejosaag.tallerbarfragment.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.alejosaag.tallerbarfragment.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MathsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MathsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MathsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtNum3;
    private EditText txtNum4;
    private EditText txtNum5;
    private Button btnComputar;

    public MathsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MathsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MathsFragment newInstance(String param1, String param2) {
        MathsFragment fragment = new MathsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_maths, container, false);

        txtNum1 = view.findViewById(R.id.txtNum1);
        txtNum2 = view.findViewById(R.id.txtNum2);
        txtNum3 = view.findViewById(R.id.txtNum3);
        txtNum4 = view.findViewById(R.id.txtNum4);
        txtNum5 = view.findViewById(R.id.txtNum5);
        btnComputar = view.findViewById(R.id.btnComputar);
        btnComputar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num1 = Integer.parseInt(txtNum1.getText().toString());
                int num2 = Integer.parseInt(txtNum2.getText().toString());
                int num3 = Integer.parseInt(txtNum3.getText().toString());
                int num4 = Integer.parseInt(txtNum4.getText().toString());
                int num5 = Integer.parseInt(txtNum5.getText().toString());

                int step1 = num1 + num2;
                int step2 = step1 - num3;
                int step3 = step2 * num4;
                int result1 = step3 / num5;

                step1 = num5 + num4;
                step2 = step1 - num3;
                step3 = step2 * num2;
                int result2 = step3 / num1;

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

                alert.setTitle("Resultado secuencias");
                alert.setMessage(String.format("Resultado primer secuencia: %d, resultado segunda secuencia: %d", result1, result2));
                alert.setCancelable(true);
                alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.show();
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
